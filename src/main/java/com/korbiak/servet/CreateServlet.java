package com.korbiak.servet;

import com.korbiak.domain.Pizza;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

public class CreateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html>" +
                "<body>");

        out.println("<form action='pizza' method='POST'>" +
                " Name: <input type='text' name='name'>\n" +
                " Description: <input type='text' name='description'>\n" +
                " <button type='submit'>Add pizza</button>" +
                "</form>");

        out.println("</body>" +
                "</html>");
    }


}
