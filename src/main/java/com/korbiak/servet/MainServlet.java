package com.korbiak.servet;

import com.korbiak.domain.Pizza;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class MainServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html>" +
                "<body>");
        out.println("<p>Hello, your choice:</p>");
        out.println("<p> <a href='create-pizza'>To Create Pizza</a> </p>");
        out.println("<p> <a href='pizzas'>To List of Pizza</a> </p>");
        out.println("</body>" +
                "</html>");
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Pizza pizza = new Pizza();
        pizza.setName(req.getParameter("name"));
        pizza.setDescription(req.getParameter("description"));
        ListServlet.company.setNewPizza(pizza);
        doGet(req, resp);
    }
}
