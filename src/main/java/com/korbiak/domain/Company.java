package com.korbiak.domain;

import java.util.LinkedList;
import java.util.List;

public class Company {

    List<Pizza> pizzaList;

    public Company() {
        pizzaList = new LinkedList<>();
    }

    public List<Pizza> getPizzaList() {
        return pizzaList;
    }

    public void setPizzaList(List<Pizza> pizzaList) {
        this.pizzaList = pizzaList;
    }

    public void setNewPizza(Pizza newPizza) {
        pizzaList.add(newPizza);
    }

    public void deletePizza(int id) {
        pizzaList.remove(id);
    }
}
